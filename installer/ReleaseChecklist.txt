
To create an installer for Distibution on Windows:

 * Install NSIS on your computer.
 * Select FORCE_INTERNAL_ZLIB in CMake for gzdoom project (to eliminate dependency on zlib.dll)
 * Build in Release mode (so vcredist_x86.exe libs will work)
 * Test:
     Does crosshair work in Rift mode?
     Do menus work in Rift mode?
     Does intermission screen work (well enough) in Rift mode?
     Does Rift mode work?
     Does green/cyan mode work?
 * Create installer by building PACKAGE target.
 * Write release notes.
 * Commit changes to github
 * Post download to site.
 * Post announcement on the web site and the usual forums.
 * Post a new demo video.
 
 